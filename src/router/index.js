import React from 'react';
import { View } from 'react-native';
import { Match, MemoryRouter as Router } from 'react-router';

import Feed from '../views/Feed';
import ExploreMap from '../views/ExploreMap';

const componentFactory = (routeName) => () => (
    <View>{routeName}</View>
);

export default props => (
    <Router>
        <View>
            <Match exactly pattern="/" component={componentFactory('Feed')}/>
            <Match exactly pattern="/map" component={componentFactory('ExploreMap')}/>
        </View>
    </Router>
);